<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('department_id')->index();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->unsignedInteger('salary');
            $table->date('employed_on');
        });
    }

    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
