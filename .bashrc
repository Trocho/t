alias dbash='docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -it -u user php bash'
alias dcomposer="docker exec -it -u user php php -d memory_limit=-1 composer.phar"
alias dphp="docker exec -it -u user php php"
alias a="dphp artisan"
