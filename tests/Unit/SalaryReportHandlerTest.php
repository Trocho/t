<?php

declare(strict_types=1);

namespace Tests\Unit;

use DateTime;
use Employee\Application\Query\Model\Department\Department;
use Employee\Application\Query\Model\Employee\Employee;
use Employee\Application\Query\Model\Employee\EmployeesFilterCriteria;
use Employee\Application\Query\Model\Employee\EmployeesFinderInterface;
use Employee\Application\Query\SalaryReportHandler;
use Employee\Application\Query\SalaryReportMessage;
use Employee\Application\Salary\SalaryReport\ReportRowCollection;
use Employee\Application\Salary\SalaryReport\SalaryReport;
use PHPUnit\Framework\TestCase;

class SalaryReportHandlerTest extends TestCase
{
    public function testBasicTest(): void
    {
        //Given
        $d = new Department('id', 'department');
        $e = new Employee('id', 'first', 'last', 10, new \DateTimeImmutable(), $d);
        $reportDate = new DateTime();
        $message = new SalaryReportMessage($reportDate, 'first_name', ['first_name']);

        $employeeFinder = $this->createMock(EmployeesFinderInterface::class);
        $salaryReport = $this->createMock(SalaryReport::class);

        $employeeFinder->expects($this->once())
            ->method('findByCriteria')
            ->with($this->callback(fn($v) => $v == new EmployeesFilterCriteria()))
            ->willReturn([$e]);

        $salaryReport->expects($this->once())
            ->method('generate')
            ->with($this->identicalTo($reportDate), $this->identicalTo($e))
            ->willReturn($except = $this->createMock(ReportRowCollection::class));

        $except->expects($this->once())
            ->method('sort')
            ->with('first_name', true)
            ->willReturnSelf();

        //When
        $handler = new SalaryReportHandler($salaryReport, $employeeFinder);
        $result = $handler($message);

        //Then
        $this->assertEquals($except, $result);
    }
}
