<?php

declare(strict_types=1);

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

class CreateEmployeeTest extends TestCase
{
    use RefreshDatabase;

    public function testEmployeeCreating(): void
    {
        $data = [
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'department_id' => Uuid::uuid1()->toString(),
            'salary' => 1234,
            'employed_on' => '2021-10-10',
        ];

        $this->assertDatabaseCount('employees', 0);
        $response = $this->post('/employees', $data);

        $response->assertStatus(302);
        $this->assertDatabaseCount('employees', 1);
        $this->assertDatabaseHas('employees', $data);
    }

    /**
     * @dataProvider toValidData
     */
    public function testValidation(array $data, array $errors): void
    {
        $response = $this->post('/employees', $data);

        $response->assertStatus(302);
        $response->assertSessionHasErrors($errors);
        $this->assertDatabaseCount('employees', 0);
    }

    public function toValidData(): array
    {
        return [
            'test required rule for all' => [
                ['first_name' => '', 'last_name' => '', 'department_id' => '', 'salary' => null, 'employed_on' => ''],
                ['first_name', 'last_name', 'department_id', 'salary', 'employed_on']
            ],
            'test data types and formats' => [
                ['first_name' => 'asd', 'last_name' => 'asd', 'department_id' => 'asd', 'salary' => 'asd', 'employed_on' => 'asd'],
                ['department_id', 'salary', 'employed_on']
            ],
        ];
    }
}
