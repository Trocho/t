<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryReport;

class ReportRow
{
    public function __construct(
        private string $firstName,
        private string $lastName,
        private string $departmentName,
        private string $benefitName,
        private int $salary,
        private int $addition,
        private int $total
    ) {
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getDepartmentName(): string
    {
        return $this->departmentName;
    }

    public function getBenefitName(): string
    {
        return $this->benefitName;
    }

    public function getSalary(): int
    {
        return $this->salary;
    }

    public function getAddition(): int
    {
        return $this->addition;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}
