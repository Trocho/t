<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryReport;

use DateTime;
use Employee\Application\Query\Model\Benefit\Benefit;
use Employee\Application\Query\Model\Benefit\BenefitsFinderInterface;
use Employee\Application\Query\Model\Employee\Employee;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculator;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorPayload;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorResult;

class SalaryReport
{
    public function __construct(
        private SalaryCalculator $salaryCalculator,
        private BenefitsFinderInterface $benefitsFinder
    ) {
    }

    public function generate(DateTime $reportDate, Employee ...$employees): ReportRowCollection
    {
        $benefits = $this->benefitsFinder->findAll();

        $data = [];
        foreach ($employees as $employee) {
            $benefit = $benefits->findForEmployee($employee);
            $calculatedSalary = $this->calculateSalary($employee, $benefit, $reportDate);

            $data[] = new ReportRow(
                $employee->getFirstName(),
                $employee->getLastName(),
                $employee->getDepartment()->getName(),
                $benefit?->getType()->getName() ?: '--',
                $calculatedSalary->getSalary(),
                $calculatedSalary->getAddition(),
                $calculatedSalary->getTotal(),
            );
        }

        return new ReportRowCollection($data);
    }

    private function calculateSalary(
        Employee $employee,
        ?Benefit $benefit,
        DateTime $reportDate
    ): SalaryCalculatorResult {
        $benefitType = $benefit?->getType()->getId() ?: '';
        $benefitAmount = $benefit?->getAmount() ?: 0;

        $payload = new SalaryCalculatorPayload(
            $employee->getSalary(),
            $benefitType,
            $benefitAmount,
            $employee->getEmployedYears($reportDate)
        );

        return $this->salaryCalculator->calculate($payload);
    }
}
