<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryReport;

use ArrayIterator;
use InvalidArgumentException;
use Webmozart\Assert\Assert;

class ReportRowCollection extends ArrayIterator
{
    public function sort(string $field, bool $type = true): self
    {
        $method = $this->prepareMethodNameForField($field);
        $type = $type ? 1 : -1;
        $rows = clone $this;

        $rows->uasort(fn(ReportRow $r1, ReportRow $r2) => $type * match (gettype($r1->$method())) {
                'integer' => $r1->$method() <=> $r2->$method(),
                'string' => strnatcasecmp($r1->$method(), $r2->$method()),
                default => 0,
            });

        return $rows;
    }

    private function prepareMethodNameForField(string $field): string
    {
        $toCamelCase = implode('', array_map('ucfirst', explode('_', $field)));
        $method = sprintf('get%s', $toCamelCase);

        method_exists($this->current(), $method) ?: throw new InvalidArgumentException(
            sprintf('No getter for %s.', $field)
        );

        return $method;
    }
}
