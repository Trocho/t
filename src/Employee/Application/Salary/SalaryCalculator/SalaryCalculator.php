<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryCalculator;

class SalaryCalculator implements SalaryCalculatorStrategyInterface
{
    public function __construct(private array $strategies)
    {
    }

    public function calculate(SalaryCalculatorPayload $calculatorPayload): SalaryCalculatorResult
    {
        return $this->resolveStrategy($calculatorPayload)->calculate($calculatorPayload);
    }

    private function resolveStrategy(SalaryCalculatorPayload $calculatorPayload): SalaryCalculatorStrategyInterface
    {
        return $this->strategies[$calculatorPayload->getBenefitType()]
            ?? new SalaryCalculatorNullStrategy();
    }
}
