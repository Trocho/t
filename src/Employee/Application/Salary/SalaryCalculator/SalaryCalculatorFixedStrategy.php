<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryCalculator;

class SalaryCalculatorFixedStrategy implements SalaryCalculatorStrategyInterface
{
    public function calculate(SalaryCalculatorPayload $calculatorPayload): SalaryCalculatorResult
    {
        $employedYears = min(10, $calculatorPayload->getEmployedYears());
        $addition = $calculatorPayload->getBenefitAmount() * $employedYears;
        $salary = $calculatorPayload->getSalary();

        return new SalaryCalculatorResult($salary, $addition, $salary + $addition);
    }
}
