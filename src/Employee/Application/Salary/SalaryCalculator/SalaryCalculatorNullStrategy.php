<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryCalculator;

class SalaryCalculatorNullStrategy implements SalaryCalculatorStrategyInterface
{
    public function calculate(SalaryCalculatorPayload $calculatorPayload): SalaryCalculatorResult
    {
        $salary = $calculatorPayload->getSalary();

        return new SalaryCalculatorResult($salary, 0, $salary);
    }
}
