<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryCalculator;

interface SalaryCalculatorStrategyInterface
{
    public function calculate(SalaryCalculatorPayload $calculatorPayload): SalaryCalculatorResult;
}
