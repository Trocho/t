<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryCalculator;

class SalaryCalculatorResult
{
    public function __construct(
        private int $salary,
        private int $addition,
        private int $total
    ) {
    }

    public function getSalary(): int
    {
        return $this->salary;
    }

    public function getAddition(): int
    {
        return $this->addition;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}
