<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryCalculator;

class SalaryCalculatorPayload
{
    public function __construct(
        private int $salary,
        private string $benefitType,
        private int $benefitAmount,
        private int $employedYears
    ) {
    }

    public function getSalary(): int
    {
        return $this->salary;
    }

    public function getBenefitType(): string
    {
        return $this->benefitType;
    }

    public function getBenefitAmount(): int
    {
        return $this->benefitAmount;
    }

    public function getEmployedYears(): int
    {
        return $this->employedYears;
    }
}
