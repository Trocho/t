<?php

declare(strict_types=1);

namespace Employee\Application\Salary\SalaryCalculator;

class SalaryCalculatorPercentStrategy implements SalaryCalculatorStrategyInterface
{
    public function calculate(SalaryCalculatorPayload $calculatorPayload): SalaryCalculatorResult
    {
        $salary = $calculatorPayload->getSalary();
        $percents = $calculatorPayload->getBenefitAmount();
        $addition = (int)round($salary * $percents / 100);

        return new SalaryCalculatorResult($salary, $addition, $salary + $addition);
    }
}
