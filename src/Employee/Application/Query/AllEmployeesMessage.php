<?php

declare(strict_types=1);

namespace Employee\Application\Query;

use Shared\Application\CQRS\QueryMessageInterface;

class AllEmployeesMessage implements QueryMessageInterface
{
    public function __construct()
    {
    }
}
