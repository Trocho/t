<?php

declare(strict_types=1);

namespace Employee\Application\Query;

use DateTime;
use Shared\Application\CQRS\QueryMessageInterface;

class SalaryReportMessage implements QueryMessageInterface
{
    public function __construct(
        private DateTime $reportDate,
        private string $sort,
        private array $filters
    ) {
    }

    public function getReportDate(): DateTime
    {
        return $this->reportDate;
    }

    public function getSort(): string
    {
        return $this->sort;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }
}
