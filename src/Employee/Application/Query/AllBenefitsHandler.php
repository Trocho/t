<?php

declare(strict_types=1);

namespace Employee\Application\Query;

use Employee\Application\Query\Model\Benefit\BenefitCollection;
use Employee\Application\Query\Model\Benefit\BenefitsFinderInterface;

class AllBenefitsHandler
{
    public function __construct(private BenefitsFinderInterface $benefitsFinder)
    {
    }

    public function __invoke(): BenefitCollection
    {
        return $this->benefitsFinder->findAll();
    }
}

