<?php

declare(strict_types=1);

namespace Employee\Application\Query;

use Shared\Application\CQRS\QueryMessageInterface;

class BenefitBaseDataMessage implements QueryMessageInterface
{
    public function __construct()
    {
    }
}
