<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Employee;

use DateTime;
use DateTimeImmutable;
use Employee\Application\Query\Model\Department\Department;

class Employee
{
    public const EMPLOYED_ON_DATE_FORMAT = 'Y-m-d';

    public function __construct(
        private string $id,
        private string $firstName,
        private string $lastName,
        private int $salary,
        private DateTimeImmutable $employedOn,
        private ?Department $department,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getSalary(): int
    {
        return $this->salary;
    }

    public function getEmployedOn(): DateTimeImmutable
    {
        return $this->employedOn;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function getEmployedYears(DateTime $dateTime = null): int
    {
        $dateTime = $dateTime ?: new DateTime();

        return (int)$dateTime->diff($this->getEmployedOn())->format('%y');
    }
}
