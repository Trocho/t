<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Employee;

interface EmployeesFinderInterface
{
    public function findAll(): array;

    public function findByCriteria(EmployeesFilterCriteria $criteria): array;
}
