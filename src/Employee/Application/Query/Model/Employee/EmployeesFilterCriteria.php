<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Employee;

class EmployeesFilterCriteria
{
    public function __construct(
        private string $firstName = '',
        private string $lastName = '',
        private string $departmentId = '',
    ) {
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getDepartmentId(): string
    {
        return $this->departmentId;
    }
}
