<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Department;

interface DepartmentsFinderInterface
{
    public function findAll(): DepartmentCollection;

    public function findById(string $id): ?Department;
}
