<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Department;

class DepartmentCollection
{
    private array $departments;

    public function __construct(Department ...$departments)
    {
        $this->departments = $departments;
    }

    public function findById(string $id): ?Department
    {
        return current(
            array_filter($this->departments, fn(Department $d) => $d->getId() === $id)
        ) ?: null;
    }

    public function values(): array
    {
        return $this->departments;
    }
}
