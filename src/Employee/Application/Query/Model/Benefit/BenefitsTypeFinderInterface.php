<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Benefit;

interface BenefitsTypeFinderInterface
{
    public function findAll(): array;

    public function findById(string $id): ?BenefitType;
}
