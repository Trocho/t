<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Benefit;

use Employee\Application\Query\Model\Employee\Employee;

class BenefitCollection
{
    private array $benefits;

    public function __construct(Benefit ...$benefits)
    {
        $this->benefits = $benefits;
    }

    /**
     * @return Benefit[]
     */
    public function values(): array
    {
        return $this->benefits;
    }

    public function findByDepartmentId(string $id): ?Benefit
    {
        return current(
            array_filter($this->benefits, fn(Benefit $b) => $id === $b->getDepartment()->getId())
        ) ?: null;
    }

    public function findForEmployee(Employee $employee): ?Benefit
    {
        $department = $employee->getDepartment();

        return $this->findByDepartmentId($department->getId());
    }
}
