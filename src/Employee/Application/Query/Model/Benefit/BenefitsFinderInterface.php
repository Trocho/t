<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Benefit;

interface BenefitsFinderInterface
{
    public function findAll(): BenefitCollection;
}
