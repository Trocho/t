<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Benefit;

use Employee\Domain\Benefit\BenefitWasCreated;
use Shared\Application\ReadModel\Projector;

interface BenefitProjector extends Projector
{
    public function projectBenefitWasCreated(BenefitWasCreated $event): void;
}
