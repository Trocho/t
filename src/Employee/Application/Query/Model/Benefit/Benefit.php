<?php

declare(strict_types=1);

namespace Employee\Application\Query\Model\Benefit;

use Employee\Application\Query\Model\Department\Department;

class Benefit
{
    public function __construct(
        private string $id,
        private int $amount,
        private ?BenefitType $type,
        private ?Department $department
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): ?BenefitType
    {
        return $this->type;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }
}
