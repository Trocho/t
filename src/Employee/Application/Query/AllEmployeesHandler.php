<?php

declare(strict_types=1);

namespace Employee\Application\Query;

use Employee\Application\Query\Model\Employee\EmployeesFinderInterface;

class AllEmployeesHandler
{
    public function __construct(private EmployeesFinderInterface $benefitsFinder)
    {
    }

    public function __invoke(): array
    {
        return $this->benefitsFinder->findAll();
    }
}
