<?php

declare(strict_types=1);

namespace Employee\Application\Query;

use Employee\Application\Query\Model\Benefit\BenefitsTypeFinderInterface;
use Employee\Application\Query\Model\Department\DepartmentsFinderInterface;
use Shared\Application\CQRS\QueryMessageInterface;

class BenefitBaseDataHandler implements QueryMessageInterface
{
    public function __construct(
        private BenefitsTypeFinderInterface $benefitsTypeFinder,
        private DepartmentsFinderInterface $departmentsFinder
    ) {
    }

    public function __invoke(): array
    {
        return [
            'departments' => $this->departmentsFinder->findAll(),
            'benefitTypes' => $this->benefitsTypeFinder->findAll(),
        ];
    }
}
