<?php

declare(strict_types=1);

namespace Employee\Application\Query;

use Employee\Application\Query\Model\Employee\EmployeesFilterCriteria;
use Employee\Application\Query\Model\Employee\EmployeesFinderInterface;
use Employee\Application\Salary\SalaryReport\ReportRowCollection;
use Employee\Application\Salary\SalaryReport\SalaryReport;

class SalaryReportHandler
{
    public function __construct(
        private SalaryReport $salaryReport,
        private EmployeesFinderInterface $employeesFinder
    ) {
    }

    public function __invoke(SalaryReportMessage $message): ReportRowCollection
    {
        $sort = $message->getSort();
        $reportDate = $message->getReportDate();
        $employees = $this->employeesFinder->findByCriteria(
            new EmployeesFilterCriteria(
                $message->getFilters()['first_name'] ?? '',
                $message->getFilters()['last_name'] ?? '',
                $message->getFilters()['department_id'] ?? ''
            )
        );

        if (empty($employees)) {
            return new ReportRowCollection();
        }

        $report = $this->salaryReport->generate($reportDate, ...$employees);

        if ($message->getSort()) {
            return $report->sort(
                str_replace('-', '', $sort),
                !str_contains($sort, '-')
            );
        }

        return $report;
    }
}
