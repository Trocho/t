<?php

declare(strict_types=1);

namespace Employee\Application\Command;

use DateTime;
use Employee\Domain\DepartmentId;
use Employee\Domain\Employee\Employee;
use Employee\Domain\Employee\EmployeeId;
use Employee\Domain\Employee\EmployeeRepositoryInterface;
use Shared\Infrastructure\Uuid;

class CreateEmployeeHandler
{
    public function __construct(private EmployeeRepositoryInterface $employeeRepository)
    {
    }

    public function __invoke(CreateEmployeeMessage $message)
    {
        $employee = new Employee(new EmployeeId(Uuid::generate()));
        $employee->create(
            $message->getFirstName(),
            $message->getLastName(),
            $message->getSalary(),
            DateTime::createFromFormat('Y-m-d', $message->getEmployedOn()),
            new DepartmentId($message->getDepartmentId())
        );

        $this->employeeRepository->save($employee);
    }
}
