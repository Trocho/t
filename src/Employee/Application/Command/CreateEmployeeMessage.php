<?php

declare(strict_types=1);

namespace Employee\Application\Command;

use Shared\Application\CQRS\CommandMessageInterface;

class CreateEmployeeMessage implements CommandMessageInterface
{
    public function __construct(
        private string $firstName,
        private string $lastName,
        private int $salary,
        private string $employedOn,
        private string $departmentId,
    ) {
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getSalary(): int
    {
        return $this->salary;
    }

    public function getEmployedOn(): string
    {
        return $this->employedOn;
    }

    public function getDepartmentId(): string
    {
        return $this->departmentId;
    }
}
