<?php

declare(strict_types=1);

namespace Employee\Application\Command;

use DomainException;
use Employee\Domain\Benefit\Benefit;
use Employee\Domain\Benefit\BenefitAmount;
use Employee\Domain\Benefit\BenefitId;
use Employee\Domain\Benefit\BenefitRepositoryInterface;
use Employee\Domain\Benefit\BenefitType;
use Employee\Domain\Benefit\BenefitUniqueValidator;
use Employee\Domain\DepartmentId;
use Shared\Domain\Version\Version;

class CreateBenefitHandler
{
    public function __construct(
        private BenefitRepositoryInterface $benefitRepository,
        private BenefitUniqueValidator $benefitUniqueValidator
    ) {
    }

    public function __invoke(CreateBenefitMessage $message): void
    {
        $amount = new BenefitAmount($message->getAmount());
        $departmentId = new DepartmentId($message->getDepartmentId());
        $benefitType = new BenefitType($message->getBenefitType());
        $benefitId = BenefitId::generate($departmentId);

        if (false === $this->benefitUniqueValidator->valid($departmentId)) {
            throw new DomainException('A benefit for selected department exists already.');
        }

        $benefit = new Benefit($benefitId, Version::first());
        $benefit->create($amount, $benefitType, $departmentId);

        $events = $benefit->getPendingEvents();

        $this->benefitRepository->save($benefit);
        //todo - dispatch $events among the domain
    }
}
