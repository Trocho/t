<?php

declare(strict_types=1);

namespace Employee\Application\Command;

use Shared\Application\CQRS\CommandMessageInterface;

class CreateBenefitMessage implements CommandMessageInterface
{
    public function __construct(
        private int $amount,
        private string $departmentId,
        private string $benefitType
    ) {
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getDepartmentId(): string
    {
        return $this->departmentId;
    }

    public function getBenefitType(): string
    {
        return $this->benefitType;
    }
}
