<?php

declare(strict_types=1);

namespace Employee\Domain;

class DepartmentId
{
    public function __construct(
        private string $id,
    ) {
    }

    public function getValue(): string
    {
        return $this->id;
    }
}
