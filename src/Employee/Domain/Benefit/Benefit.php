<?php

declare(strict_types=1);

namespace Employee\Domain\Benefit;

use Employee\Domain\DepartmentId;
use Shared\Domain\Event\DomainEvent;
use Shared\Domain\Version\Version;
use Shared\Domain\Version\VersionHelper;

class Benefit
{
    use VersionHelper;

    private DepartmentId $department;
    private BenefitType $type;
    private BenefitAmount $amount;

    private array $pendingEvents = [];

    public function __construct(private BenefitId $id, private Version $version)
    {
    }

    /**
     * @return array|DomainEvent[]
     */
    public function getPendingEvents(): array
    {
        return $this->pendingEvents;
    }

    public function flushEvents(): void
    {
        $this->pendingEvents = [];
    }

    public function id(): BenefitId
    {
        return $this->id;
    }

    public function create(
        BenefitAmount $amount,
        BenefitType $type,
        DepartmentId $department
    ): bool {
        $this->created(new BenefitWasCreated($this->id, $amount, $type, $department));

        return true;
    }

    private function created(BenefitWasCreated $event): self
    {
        $this->type = $event->getType();
        $this->amount = $event->getAmount();
        $this->department = $event->getDepartmentId();

        $this->pendingEvents[] = $event;

        return $this;
    }
}
