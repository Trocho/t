<?php

declare(strict_types=1);

namespace Employee\Domain\Benefit;

use Employee\Domain\DepartmentId;
use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;

class BenefitId
{
    private const NAMESPACE = 'dce0e828-daf2-43c3-b6b2-2af1b843719b';

    public function __construct(private string $id)
    {
        Assert::uuid($id);
    }

    public static function generate(DepartmentId $departmentId): self
    {
        return new self(
            Uuid::uuid5(self::NAMESPACE, $departmentId->getValue())->toString()
        );
    }

    public function getValue(): string
    {
        return $this->id;
    }
}
