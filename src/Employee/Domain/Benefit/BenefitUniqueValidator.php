<?php

declare(strict_types=1);

namespace Employee\Domain\Benefit;

use Employee\Domain\DepartmentId;

class BenefitUniqueValidator
{
    public function __construct(
        private BenefitRepositoryInterface $benefitRepository
    ) {
    }

    public function valid(DepartmentId $departmentId): bool
    {
        $benefitId = BenefitId::generate($departmentId);

        return false === $this->benefitRepository->existsById($benefitId);
    }
}
