<?php

declare(strict_types=1);

namespace Employee\Domain\Benefit;

class BenefitAmount
{
    public function __construct(private int $amount)
    {
    }

    public function getValue(): int
    {
        return $this->amount;
    }
}
