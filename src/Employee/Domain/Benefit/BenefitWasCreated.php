<?php

declare(strict_types=1);

namespace Employee\Domain\Benefit;

use Employee\Domain\DepartmentId;
use Shared\Domain\Event\DomainEvent;

class BenefitWasCreated implements DomainEvent
{
    public function __construct(
        private BenefitId $id,
        private BenefitAmount $amount,
        private BenefitType $type,
        private DepartmentId $departmentId
    ) {
    }

    public function getAggregateId(): string
    {
        return $this->id->getValue();
    }

    public function getDepartmentId(): DepartmentId
    {
        return $this->departmentId;
    }

    public function getType(): BenefitType
    {
        return $this->type;
    }

    public function getAmount(): BenefitAmount
    {
        return $this->amount;
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id->getValue(),
            'amount' => $this->amount->getValue(),
            'type' => $this->type->getValue(),
            'departmentId' => $this->departmentId->getValue(),
        ];
    }

    public static function deserialize(array $data): self
    {
        return new self(
            new BenefitId($data['id']),
            new BenefitAmount((int)$data['amount']),
            new BenefitType($data['type']),
            new DepartmentId($data['departmentId'])
        );
    }
}
