<?php

declare(strict_types=1);

namespace Employee\Domain\Benefit;

interface BenefitRepositoryInterface
{
    public function save(Benefit $benefit): void;

    public function existsById(BenefitId $id): bool;
}
