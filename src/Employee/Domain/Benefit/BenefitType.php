<?php

declare(strict_types=1);

namespace Employee\Domain\Benefit;

use Webmozart\Assert\Assert;

class BenefitType
{
    public const FIXED = 'fixed';
    public const PERCENT = 'percent';

    public function __construct(
        private string $type
    ) {
        Assert::inArray($type, [self::PERCENT, self::FIXED]);
    }

    public function getValue(): string
    {
        return $this->type;
    }

    public function isPercent(): bool
    {
        return $this->type === self::PERCENT;
    }
}
