<?php

declare(strict_types=1);

namespace Employee\Domain\Employee;

use DateTime;
use DomainException;
use Employee\Domain\DepartmentId;

class Employee
{
    private string $firstName;
    private string $lastName;
    private int $salary;
    private DepartmentId $departmentId;
    private DateTime $employedOn;

    public function __construct(
        private EmployeeId $id
    ) {
    }

    public function create(
        string $firstName,
        string $lastName,
        int $salary,
        DateTime $employedOn,
        DepartmentId $departmentId
    ): void {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->salary = $salary;
        $this->departmentId = $departmentId;
        $this->employedOn = $employedOn;

        $this->validation();
    }

    public static function recreate(array $data): self
    {
        $entity = new self(new EmployeeId($data['id']));

        $entity->firstName = $data['first_name'];
        $entity->lastName = $data['last_name'];
        $entity->salary = $data['salary'];
        $entity->employedOn = DateTime::createFromFormat('Y-m-d', $data['employed_on']);
        $entity->departmentId = new DepartmentId($data['department_id']);

        return $entity;
    }

    public function snapshot(): array
    {
        return [
            'id' => $this->id->getValue(),
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'salary' => $this->salary,
            'employed_on' => $this->employedOn->format('Y-m-d'),
            'department_id' => $this->departmentId->getValue(),
        ];
    }

    private function validation()
    {
        if ($this->salary <= 0) {
            throw new DomainException('Salary should be bigger than 0.');
        }
    }
}
