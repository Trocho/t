<?php

declare(strict_types=1);

namespace Employee\Domain\Employee;

use Webmozart\Assert\Assert;

class EmployeeId
{
    public function __construct(private string $id)
    {
        Assert::uuid($id);
    }

    public function getValue(): string
    {
        return $this->id;
    }
}
