<?php

declare(strict_types=1);

namespace Employee\Domain\Employee;

interface EmployeeRepositoryInterface
{
    public function save(Employee $employee): void;
}
