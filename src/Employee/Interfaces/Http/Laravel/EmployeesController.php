<?php

declare(strict_types=1);

namespace Employee\Interfaces\Http\Laravel;

use Employee\Application\Command\CreateEmployeeMessage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Shared\Infrastructure\CQRS\CommandBus;

class EmployeesController
{
    private const RULES = [
        'first_name' => 'required',
        'last_name' => 'required',
        'department_id' => 'required|uuid',
        'salary' => 'required|numeric',
        'employed_on' => 'required|date_format:Y-m-d',
    ];

    public function __construct(
        private CommandBus $commandBus
    ) {
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate(self::RULES);

        $message = new CreateEmployeeMessage(
            (string)$request->first_name,
            (string)$request->last_name,
            (int)$request->salary,
            (string)$request->employed_on,
            (string)$request->department_id
        );

        $this->commandBus->handle($message);

        return redirect()->route('home');
    }
}
