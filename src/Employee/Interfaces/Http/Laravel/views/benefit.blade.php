@php
    /** @var \Employee\Application\Query\Model\Benefit\Benefit $benefit */
@endphp

<h2>Benefits</h2>
<table>
    <tr>
        <th>Id</th>
        <th>Department</th>
        <th>Amount</th>
        <th>Type</th>
    </tr>

    @foreach($benefits->values() as $benefit)
        <tr>
            <td>{{ $benefit->getId() }}</td>
            <td>{{ $benefit->getDepartment()->getName() }}</td>
            <td>{{ $benefit->getAmount() }}</td>
            <td>{{ $benefit->getType()->getName() }}</td>
        </tr>
    @endforeach
</table>
@empty($benefits->values())
    -- empty --
@endempty
<h2>Add a new benefit</h2>
<form method="POST" action="{{ route('benefits.store') }}">
    {{ csrf_field() }}
    {{ method_field('POST') }}

    <div>
        <label>Department</label>
        <select name="department_id">
            @foreach($departments->values() as $department)
                <option value="{{ $department->getId() }}">{{ $department->getName() }}</option>
            @endforeach
        </select>
    </div>

    <div>
        <label>Type</label>
        <select name="type">
            @foreach($benefitTypes as $type)
                <option value="{{ $type->getId() }}">{{ $type->getName() }}</option>
            @endforeach
        </select>
    </div>

    <div>
        <label>Amount</label>
        <input type="number" min="1" name="amount">
    </div>

    <div>
        <button type="submit">Submit</button>
    </div>
</form>
