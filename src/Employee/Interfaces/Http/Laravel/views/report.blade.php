<h2>Current month's salary report</h2>
<form action="">
    <table>
        <tr>
            <th>@include('employee::sort-field', ['name' => 'First name', 'field' => 'first_name'])</th>
            <th>@include('employee::sort-field', ['name' => 'Last name', 'field' => 'last_name'])</th>
            <th>@include('employee::sort-field', ['name' => 'Department', 'field' => 'department_name'])</th>
            <th>@include('employee::sort-field', ['name' => 'Salary', 'field' => 'salary'])</th>
            <th>@include('employee::sort-field', ['name' => 'Addition', 'field' => 'addition'])</th>
            <th>@include('employee::sort-field', ['name' => 'Benefit', 'field' => 'benefit_name'])</th>
            <th>@include('employee::sort-field', ['name' => 'Total', 'field' => 'total'])</th>
        </tr>
        <form method="get" action="">
            <input type="hidden" name="sort" value="{{ request('sort') }}">
            <th><input onfocusout="this.form.submit()" type="text" name="filter[first_name]"
                       value="{{ request('filter.first_name') }}"></th>
            <th><input onfocusout="this.form.submit()" type="text" name="filter[last_name]"
                       value="{{ request('filter.last_name') }}"></th>
            <th>
                <select style="width: 100%" onchange="this.form.submit()" name="filter[department_id]">
                    <option></option>
                    @foreach($departments->values() as $department)
                        <option
                            {{ request('filter.department_id') ===  $department->getId() ? 'selected' : '' }} value="{{ $department->getId() }}">
                            {{ $department->getName() }}
                        </option>
                    @endforeach
                </select>
            </th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </form>
        @foreach($report as $row)
            <tr>
                <td>{{ $row->getFirstName() }}</td>
                <td>{{ $row->getLastName() }}</td>
                <td>{{ $row->getDepartmentName() }}</td>
                <td>{{ $row->getSalary() }}</td>
                <td>{{ $row->getAddition() }}</td>
                <td>{{ $row->getBenefitName() }}</td>
                <td>{{ $row->getTotal() }}</td>
            </tr>
        @endforeach
    </table>
</form>
