<form method="GET">
    @foreach(request('filter', []) as $key => $filter )
        <input type="hidden" name="filter[{{ $key }}]" value="{{ $filter }}">
    @endforeach

    <div class="sort">
        <select style="width: 100%" onchange="this.form.submit()" name="sort">
            <option value="">{{ $name ?? '' }}</option>
            <option {{ $field === request('sort') ? 'selected' : '' }} value="{{ $field }}">{{ $name ?? '' }}⬆</option>
            <option {{ "-$field" === request('sort') ? 'selected' : '' }} value="-{{ $field }}">{{ $name ?? '' }}⬇
            </option>
        </select>
    </div>
</form>
