@php
    /** @var \Employee\Application\Query\Model\Employee\Employee $employee */
@endphp
<h2>Employees</h2>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Salary</th>
        <th>Department</th>
        <th>Employed on</th>
    </tr>
    @foreach($employees as $employee)
        <tr>
            <td>{{ $employee->getId() }}</td>
            <td>{{ $employee->getFirstName() }} {{ $employee->getLastName() }}</td>
            <td>{{ $employee->getSalary() }}</td>
            <td>{{ $employee->getDepartment()?->getName() ?: '--' }}</td>
            <td>{{ $employee->getEmployedOn()->format('Y-m-d') }}</td>
        </tr>
    @endforeach
</table>
@empty($employees)
    -- empty --
@endempty
<h2>Add a new employee</h2>
<form method="POST" action="{{ route('employees.store') }}">
    {{ csrf_field() }}
    {{ method_field('POST') }}

    <div>
        <label>First name</label>
        <input type="text" name="first_name">
    </div>

    <div>
        <label>Last name</label>
        <input type="text" name="last_name">
    </div>

    <div>
        <label>Salary</label>
        <input type="number" min="1" name="salary">
    </div>

    <div>
        <label>Employed on</label>
        <input type="date" dataformatas="yyyy-mm-dd" name="employed_on">
    </div>

    <div>
        <label>Department</label>
        <select name="department_id">
            @foreach($departments->values() as $department)
                <option value="{{ $department->getId() }}">{{ $department->getName() }}</option>
            @endforeach
        </select>
    </div>

    <div>
        <button type="submit">Submit</button>
    </div>
</form>
