@php
    /** @var \Employee\Application\Salary\SalaryReport\ReportRow $row */
@endphp

@extends('shared::layout')

@section('content')
    {{ $errors }}

    @include('employee::benefit')
    <hr>
    @include('employee::employee')
    <hr>
    @include('employee::report')
@endsection
