<?php

declare(strict_types=1);

namespace Employee\Interfaces\Http\Laravel;

use Employee\Application\Command\CreateBenefitMessage;
use Employee\Domain\Benefit\BenefitUniqueValidator;
use Employee\Domain\DepartmentId;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Shared\Infrastructure\CQRS\CommandBus;

class BenefitsController
{
    private const RULES = [
        'amount' => 'required|numeric|min:1',
        'department_id' => 'required|uuid',
        'type' => 'required'
    ];

    public function __construct(
        private CommandBus $commandBus
    ) {
    }

    public function store(Request $request, BenefitUniqueValidator $benefitUniqueValidator): RedirectResponse
    {
        $request->validate(self::RULES);

        if (false === $benefitUniqueValidator->valid(new DepartmentId($request->department_id))) {
            throw ValidationException::withMessages(
                ['department_id' => __('A benefit for selected department exists already.')]
            );
        }

        $message = new CreateBenefitMessage(
            (int)$request->amount,
            (string)$request->department_id,
            (string)$request->type,
        );

        $this->commandBus->handle($message);

        return redirect()->route('home');
    }
}
