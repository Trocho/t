<?php

declare(strict_types=1);

namespace Employee\Interfaces\Http\Laravel;

use DateTime;
use Employee\Application\Query\AllBenefitsMessage;
use Employee\Application\Query\AllEmployeesMessage;
use Employee\Application\Query\BenefitBaseDataMessage;
use Employee\Application\Query\SalaryReportMessage;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Shared\Application\CQRS\QueryBusInterface;

class HomeController
{
    public function __construct(
        private QueryBusInterface $queryBus
    ) {
    }

    public function index(Request $request): View
    {
        $now = new DateTime('now');
        $reportDate = (new DateTime())->modify(
            sprintf('last day of %s %s 23:59:59', $now->format('F'), $now->format('Y'))
        );

        $message = new SalaryReportMessage(
            $reportDate,
            $request->sort ?: '',
            (array)$request->filter ?: [],
        );

        $report = $this->queryBus->handle($message);
        $data = $this->queryBus->handle(new BenefitBaseDataMessage());
        $benefits = $this->queryBus->handle(new AllBenefitsMessage());
        $employees = $this->queryBus->handle(new AllEmployeesMessage());

        return view('employee::home')->with(
            $data + compact('benefits', 'employees', 'report')
        );
    }
}
