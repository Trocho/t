<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Query\Projector;

use Employee\Application\Query\Model\Benefit\Benefit;
use Employee\Application\Query\Model\Benefit\BenefitProjector;
use Employee\Application\Query\Model\Benefit\BenefitsTypeFinderInterface;
use Employee\Application\Query\Model\Department\DepartmentsFinderInterface;
use Employee\Domain\Benefit\BenefitWasCreated;
use ReflectionClass;
use Shared\Domain\Event\DomainEvent;

class StaticBenefitProjector implements BenefitProjector
{
    private ?Benefit $benefit;

    public function __construct(
        private BenefitsTypeFinderInterface $benefitsTypeFinder,
        private DepartmentsFinderInterface $departmentsFinder
    ) {
    }

    public function projectBenefitWasCreated(BenefitWasCreated $event): void
    {
        $type = $this->benefitsTypeFinder->findById($event->getType()->getValue());
        $department = $this->departmentsFinder->findById($event->getDepartmentId()->getValue());

        $this->benefit = new Benefit(
            $event->getAggregateId(),
            $event->getAmount()->getValue(),
            $type,
            $department,
        );
    }

    public function project(DomainEvent ...$eventStream): void
    {
        $this->benefit = null;

        foreach ($eventStream as $event) {
            $reflection = new ReflectionClass($event);
            $this->{'project' . $reflection->getShortName()}($event);
        }
    }

    public function __invoke(): ?Benefit
    {
        return $this->benefit;
    }
}
