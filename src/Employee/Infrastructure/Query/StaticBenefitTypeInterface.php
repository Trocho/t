<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Query;

use Employee\Application\Query\Model\Benefit\BenefitsTypeFinderInterface;
use Employee\Application\Query\Model\Benefit\BenefitType;
use Employee\Domain\Benefit\BenefitType as DomainBenefitType;

class StaticBenefitTypeInterface implements BenefitsTypeFinderInterface
{
    public function findAll(): array
    {
        return [
            new BenefitType(DomainBenefitType::FIXED, 'Fixed'),
            new BenefitType(DomainBenefitType::PERCENT, 'Percent'),
        ];
    }

    public function findById(string $id): ?BenefitType
    {
        return current(
            array_filter($this->findAll(), fn(BenefitType $t) => $id === $t->getId())
        ) ?: null;
    }
}
