<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Query;

use DateTimeImmutable;
use Employee\Application\Query\Model\Department\DepartmentsFinderInterface;
use Employee\Application\Query\Model\Employee\Employee;
use Employee\Application\Query\Model\Employee\EmployeesFilterCriteria;
use Employee\Application\Query\Model\Employee\EmployeesFinderInterface;
use Illuminate\Database\Connection;

class MysqlEmployeesFinder implements EmployeesFinderInterface
{
    public function __construct(
        private Connection $connection,
        private DepartmentsFinderInterface $departmentsFinder
    ) {
    }

    public function findAll(): array
    {
        return $this->prepareResults(
            $this->connection->table('employees')->get()->toArray()
        );
    }

    public function findByCriteria(EmployeesFilterCriteria $criteria): array
    {
        $criteria = array_filter(
            [
                'first_name' => $criteria->getFirstName(),
                'last_name' => $criteria->getLastName(),
                'department_id' => $criteria->getDepartmentId(),
            ]
        );

        return $this->prepareResults(
            $this->connection->table('employees')->where($criteria)->get()->toArray()
        );
    }

    private function prepareResults(array $list): array
    {
        $departments = $this->departmentsFinder->findAll();

        return array_map(
            fn($data) => new Employee(
                $data->id,
                $data->first_name,
                $data->last_name,
                $data->salary,
                DateTimeImmutable::createFromFormat(Employee::EMPLOYED_ON_DATE_FORMAT, $data->employed_on),
                $departments->findById($data->department_id)
            ),
            $list
        );
    }
}
