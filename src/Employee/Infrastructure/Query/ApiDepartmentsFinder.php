<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Query;

use Department\Application\Query\Model\Department\Department as BaseDepartmentAlias;
use Department\Interfaces\Api\DepartmentsApi;
use Employee\Application\Query\Model\Department\Department;
use Employee\Application\Query\Model\Department\DepartmentCollection;
use Employee\Application\Query\Model\Department\DepartmentsFinderInterface;

class ApiDepartmentsFinder implements DepartmentsFinderInterface
{
    public function __construct(private DepartmentsApi $departmentsApi)
    {
    }

    public function findAll(): DepartmentCollection
    {
        return new DepartmentCollection(
            ... array_map(
                fn(BaseDepartmentAlias $d) => new Department($d->getId(), $d->getName()),
                $this->departmentsApi->findAll()->values()
            )
        );
    }

    public function findById(string $id): ?Department
    {
        return current(
            array_filter($this->findAll()->values(), fn(Department $d) => $id === $d->getId())
        ) ?: null;
    }
}
