<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Query;

use Employee\Application\Query\Model\Benefit\BenefitCollection;
use Employee\Application\Query\Model\Benefit\BenefitsFinderInterface;
use Employee\Infrastructure\Domain\Repository\MysqlEventStoreBenefitRepository;
use Employee\Infrastructure\Query\Projector\StaticBenefitProjector;

class StaticBenefitsFinder implements BenefitsFinderInterface
{
    public function __construct(
        private StaticBenefitProjector $staticBenefitProjector,
        private MysqlEventStoreBenefitRepository $benefitRepository
    ) {
    }

    /**
     * @return BenefitCollection
     */
    public function findAll(): BenefitCollection
    {
        $eventsGroupedByAggregate = $this->benefitRepository->domainEventsForAll();

        return new BenefitCollection(
            ... array_map(
                function (array $events) {
                    $projector = $this->staticBenefitProjector;
                    $projector->project(...$events);

                    return $projector();
                },
                $eventsGroupedByAggregate
            )
        );
    }
}
