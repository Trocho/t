<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Domain\Repository;

use Employee\Domain\Employee\Employee;
use Employee\Domain\Employee\EmployeeRepositoryInterface;
use Illuminate\Database\Connection;

class MysqlEmployeeRepository implements EmployeeRepositoryInterface
{
    private string $table = 'employees';

    public function __construct(private Connection $connection)
    {
    }

    public function save(Employee $employee): void
    {
        $this->connection->table($this->table)->insert($employee->snapshot());
    }
}
