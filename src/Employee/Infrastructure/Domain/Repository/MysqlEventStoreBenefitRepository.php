<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Domain\Repository;

use Employee\Domain\Benefit\Benefit;
use Employee\Domain\Benefit\BenefitId;
use Employee\Domain\Benefit\BenefitRepositoryInterface;
use Employee\Domain\Benefit\BenefitWasCreated;
use Illuminate\Database\Connection;
use Ramsey\Uuid\Uuid;
use Shared\Domain\Event\DomainEvent;
use Shared\Domain\EventSourcing\EventsBidirectionalMap;
use Shared\Domain\EventSourcing\StoreEvent;

class MysqlEventStoreBenefitRepository implements BenefitRepositoryInterface
{
    private string $table = 'benefits';

    private EventsBidirectionalMap $bidirectionalEventMap;

    public function __construct(private Connection $connection)
    {
        $this->bidirectionalEventMap = new EventsBidirectionalMap(
            [
                'benefit_created' => BenefitWasCreated::class,
            ]
        );
    }

    public function save(Benefit $benefit): void
    {
        $currentVersion = $benefit->version()->getValue();

        $stream = [];
        foreach ($benefit->getPendingEvents() as $event) {
            $stream[] = (new StoreEvent(
                Uuid::uuid1()->toString(),
                $benefit->id()->getValue(),
                $currentVersion++,
                $this->bidirectionalEventMap->getEventType($event),
                json_encode($event->serialize())
            ))->serialize();
        }

        $this->connection->table($this->table)->insert($stream);
    }

    public function existsById(BenefitId $id): bool
    {
        return $this->connection->table($this->table)->where('aggregate_id', $id->getValue())->exists();
    }

    public function domainEventsForAll(): array
    {
        return $this->connection->table($this->table)->get()
            ->map(fn($d) => StoreEvent::deserialize((array)$d))
            ->map(fn(StoreEvent $e) => $this->bringToDomainEvent($e))
            ->groupBy(fn(DomainEvent $event) => $event->getAggregateId())
            ->toArray();
    }

    private function bringToDomainEvent(StoreEvent $storeEvent)
    {
        return $this->bidirectionalEventMap->getDomainEvent($storeEvent)::deserialize(
            json_decode($storeEvent->getEventData(), true)
        );
    }
}
