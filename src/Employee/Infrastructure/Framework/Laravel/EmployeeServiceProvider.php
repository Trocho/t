<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Framework\Laravel;

use Employee\Application\Command\CreateBenefitHandler;
use Employee\Application\Command\CreateBenefitMessage;
use Employee\Application\Command\CreateEmployeeHandler;
use Employee\Application\Command\CreateEmployeeMessage;
use Employee\Application\Query\AllBenefitsHandler;
use Employee\Application\Query\AllBenefitsMessage;
use Employee\Application\Query\AllEmployeesHandler;
use Employee\Application\Query\AllEmployeesMessage;
use Employee\Application\Query\BenefitBaseDataHandler;
use Employee\Application\Query\BenefitBaseDataMessage;
use Employee\Application\Query\Model\Benefit\BenefitsFinderInterface;
use Employee\Application\Query\Model\Benefit\BenefitsTypeFinderInterface;
use Employee\Application\Query\Model\Department\DepartmentsFinderInterface;
use Employee\Application\Query\Model\Employee\EmployeesFinderInterface;
use Employee\Application\Query\SalaryReportHandler;
use Employee\Application\Query\SalaryReportMessage;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculator;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorFixedStrategy;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorPercentStrategy;
use Employee\Domain\Benefit\BenefitRepositoryInterface;
use Employee\Domain\Benefit\BenefitType;
use Employee\Domain\Employee\EmployeeRepositoryInterface;
use Employee\Infrastructure\Domain\Repository\MysqlEmployeeRepository;
use Employee\Infrastructure\Domain\Repository\MysqlEventStoreBenefitRepository;
use Employee\Infrastructure\Query\ApiDepartmentsFinder;
use Employee\Infrastructure\Query\MysqlEmployeesFinder;
use Employee\Infrastructure\Query\StaticBenefitsFinder;
use Employee\Infrastructure\Query\StaticBenefitTypeInterface;
use Employee\Interfaces\Http\Laravel\BenefitsController;
use Employee\Interfaces\Http\Laravel\EmployeesController;
use Employee\Interfaces\Http\Laravel\HomeController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Shared\Infrastructure\CQRS\BusRegisterHelperForServiceProviders;

class EmployeeServiceProvider extends ServiceProvider
{
    use BusRegisterHelperForServiceProviders;

    protected function queriesList(): array
    {
        return [
            SalaryReportMessage::class => SalaryReportHandler::class,
            BenefitBaseDataMessage::class => BenefitBaseDataHandler::class,
            AllBenefitsMessage::class => AllBenefitsHandler::class,
            AllEmployeesMessage::class => AllEmployeesHandler::class,
        ];
    }

    protected function commandList(): array
    {
        return [
            CreateBenefitMessage::class => CreateBenefitHandler::class,
            CreateEmployeeMessage::class => CreateEmployeeHandler::class,
        ];
    }

    public function boot(): void
    {
        $this->registerBuses();
        $this->loadViewsFrom(realpath(__DIR__ . '/../../../Interfaces/Http/Laravel/views'), 'employee');

        Route::middleware(['web'])->group(function (Router $router) {
            $router->get('/', [HomeController::class, 'index'])->name('home');
            $router->resource('/benefits', BenefitsController::class)->only('store');
            $router->resource('/employees', EmployeesController::class)->only('store');
        });
    }

    public function register(): void
    {
        $this->app->bind(BenefitsFinderInterface::class, StaticBenefitsFinder::class);
        $this->app->bind(DepartmentsFinderInterface::class, ApiDepartmentsFinder::class);
        $this->app->bind(BenefitsTypeFinderInterface::class, StaticBenefitTypeInterface::class);
        $this->app->bind(EmployeesFinderInterface::class, MysqlEmployeesFinder::class);

        $this->app->bind(BenefitRepositoryInterface::class, MysqlEventStoreBenefitRepository::class);
        $this->app->bind(EmployeeRepositoryInterface::class, MysqlEmployeeRepository::class);

        $this->app->bind(SalaryCalculator::class, function () {
            $strategies = [
                BenefitType::PERCENT => $this->app->get(SalaryCalculatorPercentStrategy::class),
                BenefitType::FIXED => $this->app->get(SalaryCalculatorFixedStrategy::class),
            ];

            return new SalaryCalculator($strategies);
        });
    }
}
