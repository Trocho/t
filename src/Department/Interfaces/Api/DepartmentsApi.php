<?php

declare(strict_types=1);

namespace Department\Interfaces\Api;

use Department\Application\Query\AllDepartmentsMessage;
use Department\Application\Query\Model\Department\DepartmentCollection;
use Shared\Application\CQRS\QueryBusInterface;
use Shared\Interfaces\Api\ApiInterface;

class DepartmentsApi implements ApiInterface
{
    public function __construct(private QueryBusInterface $queryBus)
    {
    }

    public function findAll(): DepartmentCollection
    {
        return $this->queryBus->handle(new AllDepartmentsMessage());
    }
}
