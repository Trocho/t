<?php

declare(strict_types=1);

namespace Department\Application\Query;

use Department\Application\Query\Model\Department\DepartmentCollection;
use Department\Application\Query\Model\Department\DepartmentsFinderInterface;

class AllDepartmentsHandler
{
    public function __construct(private DepartmentsFinderInterface $departmentsFinder)
    {
    }

    public function __invoke(AllDepartmentsMessage $message): DepartmentCollection
    {
        return $this->departmentsFinder->findAll();
    }
}
