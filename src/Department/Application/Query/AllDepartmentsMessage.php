<?php

declare(strict_types=1);

namespace Department\Application\Query;

use Shared\Application\CQRS\QueryMessageInterface;

class AllDepartmentsMessage implements QueryMessageInterface
{

}
