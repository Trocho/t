<?php

declare(strict_types=1);

namespace Department\Application\Query\Model\Department;

class DepartmentCollection
{
    private array $departments;

    public function __construct(Department ...$departments)
    {
        $this->departments = $departments;
    }

    /**
     * @return Department[]
     */
    public function values(): array
    {
        return $this->departments;
    }

    public function findById(string $id): ?Department
    {
        return current(
            array_filter($this->values(), fn(Department $d) => $d->getId() === $id)
        ) ?: null;
    }
}
