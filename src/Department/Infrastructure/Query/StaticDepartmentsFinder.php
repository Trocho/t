<?php

declare(strict_types=1);

namespace Department\Infrastructure\Query;

use Department\Application\Query\Model\Department\Department;
use Department\Application\Query\Model\Department\DepartmentCollection;
use Department\Application\Query\Model\Department\DepartmentsFinderInterface;

class StaticDepartmentsFinder implements DepartmentsFinderInterface
{
    public function findAll(): DepartmentCollection
    {
        return new DepartmentCollection(
            ... [
                new Department('6631ada7-4323-4cfb-ad48-ef222c27192e', 'C-level'),
                new Department('4acc7fa5-ea8e-4b71-b93c-9e3973caf3cd', 'Sales'),
                new Department('b540daa9-a21c-4ed3-8e0d-e60232553217', 'Marketing'),
            ]
        );
    }

    public function findById(string $id): ?Department
    {
        return $this->findAll()->findById($id);
    }
}
