<?php

declare(strict_types=1);

namespace Department\Infrastructure\Framework\Laravel;

use Department\Application\Query\AllDepartmentsHandler;
use Department\Application\Query\AllDepartmentsMessage;
use Department\Application\Query\Model\Department\DepartmentsFinderInterface;
use Department\Infrastructure\Query\StaticDepartmentsFinder;
use Illuminate\Support\ServiceProvider;
use Shared\Infrastructure\CQRS\BusRegisterHelperForServiceProviders;

class DepartmentServiceProvider extends ServiceProvider
{
    use BusRegisterHelperForServiceProviders;

    protected function queriesList(): array
    {
        return [
            AllDepartmentsMessage::class => AllDepartmentsHandler::class,
        ];
    }

    protected function commandList(): array
    {
        return [];
    }

    public function boot(): void
    {
        $this->registerBuses();
    }

    public function register(): void
    {
        $this->app->bind(DepartmentsFinderInterface::class, StaticDepartmentsFinder::class);
    }
}
