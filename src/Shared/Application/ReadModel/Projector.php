<?php

declare(strict_types=1);

namespace Shared\Application\ReadModel;

use Shared\Domain\Event\DomainEvent;

interface Projector
{
    public function project(DomainEvent ...$eventStream): void;
}
