<?php

declare(strict_types=1);

namespace Shared\Application\CQRS;

interface BusInterface
{
    public function register(string $message, string $handler);
}
