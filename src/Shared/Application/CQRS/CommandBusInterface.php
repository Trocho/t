<?php

declare(strict_types=1);

namespace Shared\Application\CQRS;

interface CommandBusInterface
{
    public function handle(CommandMessageInterface $message);
}
