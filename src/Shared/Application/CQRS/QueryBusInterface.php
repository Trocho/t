<?php

declare(strict_types=1);

namespace Shared\Application\CQRS;

interface QueryBusInterface
{
    public function handle(QueryMessageInterface $message);
}
