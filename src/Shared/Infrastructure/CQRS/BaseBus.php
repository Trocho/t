<?php

declare(strict_types=1);

namespace Shared\Infrastructure\CQRS;

use Shared\Application\CQRS\BusInterface;

abstract class BaseBus implements BusInterface
{
    protected array $handlers = [];

    abstract protected function validRegistration(string $message, string $handler): void;

    public function register(string $message, string $handler): void
    {
        $this->validRegistration($message, $handler);

        $this->handlers[$message] = $handler;
    }
}
