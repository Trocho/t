<?php

declare(strict_types=1);

namespace Shared\Infrastructure\CQRS;

use InvalidArgumentException;
use Shared\Application\CQRS\BusInterface;
use Shared\Application\CQRS\CommandBusInterface;
use Shared\Application\CQRS\CommandMessageInterface;

class CommandBus extends BaseBus implements CommandBusInterface, BusInterface
{
    protected function validRegistration(string $message, string $handler): void
    {
        class_implements($message)[CommandMessageInterface::class] ?? throw new InvalidArgumentException(
            'Given class should implement interface of ' . CommandMessageInterface::class
        );
    }

    public function handle(CommandMessageInterface $message): void
    {
        $commandName = get_class($message);

        $handler = $this->handlers[$commandName] ?? throw new InvalidArgumentException(
                "No handler was found for given message [{$commandName}]"
            );

        class_exists($handler) or throw new InvalidArgumentException(
            "Handle for given message does not exist [{$handler}]"
        );

        app($handler)($message);
    }
}
