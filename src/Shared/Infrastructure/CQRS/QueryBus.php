<?php

declare(strict_types=1);

namespace Shared\Infrastructure\CQRS;

use InvalidArgumentException;
use Shared\Application\CQRS\BusInterface;
use Shared\Application\CQRS\CommandMessageInterface;
use Shared\Application\CQRS\QueryBusInterface;
use Shared\Application\CQRS\QueryMessageInterface;

class QueryBus extends BaseBus implements QueryBusInterface, BusInterface
{
    protected function validRegistration(string $message, string $handler): void
    {
        class_implements($message)[QueryMessageInterface::class] ?? throw new InvalidArgumentException(
            'Given class should implement interface of ' . CommandMessageInterface::class
        );
    }

    public function handle(QueryMessageInterface $message)
    {
        $queryName = get_class($message);

        $handler = $this->handlers[$queryName] ?? throw new InvalidArgumentException(
                "No handler was found for given message [{$queryName}]"
            );

        class_exists($handler) or throw new InvalidArgumentException(
            "Handle for given message does not exist [{$handler}]"
        );

        return app($handler)($message);
    }
}
