<?php

declare(strict_types=1);

namespace Shared\Infrastructure\CQRS;

trait BusRegisterHelperForServiceProviders
{
    abstract protected function commandList(): array;

    abstract protected function queriesList(): array;

    public function registerBuses(): void
    {
        $queryBus = $this->app->get(QueryBus::class);
        $commandBus = $this->app->get(CommandBus::class);

        foreach ($this->queriesList() as $message => $handler) {
            $queryBus->register($message, $handler);
        }

        foreach ($this->commandList() as $message => $handler) {
            $commandBus->register($message, $handler);
        }
    }
}
