<?php

declare(strict_types=1);

namespace Shared\Infrastructure;

class Uuid
{
    public static function generate(): string
    {
        return \Ramsey\Uuid\Uuid::uuid1()->toString();
    }
}
