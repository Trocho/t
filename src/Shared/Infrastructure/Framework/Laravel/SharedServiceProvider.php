<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Framework\Laravel;

use Illuminate\Support\ServiceProvider;
use Shared\Application\CQRS\CommandBusInterface;
use Shared\Application\CQRS\QueryBusInterface;
use Shared\Infrastructure\CQRS\CommandBus;
use Shared\Infrastructure\CQRS\QueryBus;

class SharedServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/../../../Interfaces/Laravel/Http/views'), 'shared');
    }

    public function register()
    {
        $this->app->singleton(CommandBus::class);
        $this->app->bind(CommandBusInterface::class, CommandBus::class);

        $this->app->singleton(QueryBus::class);
        $this->app->bind(QueryBusInterface::class, QueryBus::class);
    }
}
