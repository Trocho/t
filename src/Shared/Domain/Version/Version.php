<?php

declare(strict_types=1);

namespace Shared\Domain\Version;

class Version
{
    public function __construct(private int $version)
    {
    }

    public static function first(): self
    {
        return new Version(1);
    }

    public function getValue(): int
    {
        return $this->version;
    }
}
