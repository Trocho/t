<?php

declare(strict_types=1);

namespace Shared\Domain\Version;

trait VersionHelper
{
    private Version $version;

    public function version(): Version
    {
        return $this->version;
    }
}
