<?php

declare(strict_types=1);

namespace Shared\Domain\EventSourcing;

use InvalidArgumentException;
use Shared\Domain\Event\DomainEvent;
use Throwable;

class UnsupportedDomainEventException extends InvalidArgumentException
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function noEventType(DomainEvent $event): self
    {
        return new self(sprintf('There is no an event type for [%s].', get_class($event)));
    }
}
