<?php

declare(strict_types=1);

namespace Shared\Domain\EventSourcing;

use InvalidArgumentException;
use Throwable;

class UnsupportedEventTypeException extends InvalidArgumentException
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function noDomainEvent(StoreEvent $event): self
    {
        return new self(sprintf('There is no a domain event for [%s].', $event->getEventType()));
    }
}
