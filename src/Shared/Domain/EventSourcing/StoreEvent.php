<?php

declare(strict_types=1);

namespace Shared\Domain\EventSourcing;

class StoreEvent
{
    public function __construct(
        private string $eventId,
        private string $aggregateId,
        private int $version,
        private string $eventType,
        private string $eventData
    ) {
    }

    public function getEventData(): string
    {
        return $this->eventData;
    }

    public static function deserialize(array $data): self
    {
        return new self(
            $data['event_id'],
            $data['aggregate_id'],
            $data['version'],
            $data['event_type'],
            $data['event_data'],
        );
    }

    public function serialize(): array
    {
        return [
            'event_id' => $this->eventId,
            'aggregate_id' => $this->aggregateId,
            'version' => $this->version,
            'event_type' => $this->eventType,
            'event_data' => $this->eventData,
        ];
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }
}
