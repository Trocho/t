<?php

declare(strict_types=1);

namespace Shared\Domain\EventSourcing;

use Shared\Domain\Event\DomainEvent;

class EventsBidirectionalMap
{
    public function __construct(private array $map)
    {
    }

    public function getEventType(DomainEvent $event): string
    {
        return array_flip($this->map)[get_class($event)]
            ?? throw UnsupportedDomainEventException::noEventType($event);
    }

    public function getDomainEvent(StoreEvent $event): string
    {
        return $this->map[$event->getEventType()]
            ?? throw UnsupportedEventTypeException::noDomainEvent($event);
    }
}
