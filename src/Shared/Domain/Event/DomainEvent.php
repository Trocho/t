<?php

declare(strict_types=1);

namespace Shared\Domain\Event;

interface DomainEvent
{
    public function getAggregateId(): string;

    public function serialize(): array;

    public static function deserialize(array $data): self;
}
