## Start page
```
http://localhost/
```

## Requirements
* docker
* docker-compose

## How to add a new department?
Add a new record in `Department\Infrastructure\Query\StaticDepartmentsFinder`.

## Structure
* main code - `src/`
* feature and unit tests `tests/` and `spec/`

## Install and run
```
make install
```

## Run

```
make run
```
## Tests

```
make test
```
