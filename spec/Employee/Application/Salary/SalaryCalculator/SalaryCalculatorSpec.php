<?php

declare(strict_types=1);

namespace spec\Employee\Application\Salary\SalaryCalculator;

use Employee\Application\Salary\SalaryCalculator\SalaryCalculator;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorFixedStrategy;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorPayload;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorPercentStrategy;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorResult;
use PhpSpec\ObjectBehavior;

class SalaryCalculatorSpec extends ObjectBehavior
{
    private const PERCENT = 'percent';
    private const FIXED = 'fixed';

    function let() {
        $strategies = [
            self::PERCENT => new SalaryCalculatorPercentStrategy(),
            self::FIXED => new SalaryCalculatorFixedStrategy(),
        ];

        $this->beConstructedWith($strategies);
    }

    public function it_calculates_price_for_percent(): void
    {
        $payload = new SalaryCalculatorPayload(
            $salary = 1000,
            self::PERCENT,
            $amount = 10,
            $employedYears = 10
        );

        $result = $this->calculate($payload);

        $result->shouldBeAnInstanceOf(SalaryCalculatorResult::class);
        $result->getSalary()->shouldBeEqualTo($salary);
        $result->getAddition()->shouldBeEqualTo($addition = $salary * $amount / 100);
        $result->getTotal()->shouldBeEqualTo($addition + $salary);
    }

    public function it_calculates_price_for_fixed(): void
    {
        $payload = new SalaryCalculatorPayload(
            $salary = 1000,
            self::FIXED,
            $amount = 10,
            $employedYears = 10
        );

        $result = $this->calculate($payload);

        $result->getSalary()->shouldBeEqualTo($salary);
        $result->getAddition()->shouldBeEqualTo($addition = $amount * $employedYears);
        $result->getTotal()->shouldBeEqualTo($addition + $salary);
    }

    public function it_does_calculate_price_for_wrong_type(): void
    {
        $payload = new SalaryCalculatorPayload(
            $salary = 1000,
            $type = 'gdgdrgd',
            $amount = 10,
            $employedYears = 10
        );

        $result = $this->calculate($payload);

        $result->getSalary()->shouldBeEqualTo($salary);
        $result->getAddition()->shouldBeEqualTo(0);
        $result->getTotal()->shouldBeEqualTo($salary);
    }
}
