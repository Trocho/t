<?php

declare(strict_types=1);

namespace spec\Employee\Application\Salary\SalaryReport;

use Employee\Application\Query\Model\Benefit\Benefit;
use Employee\Application\Query\Model\Benefit\BenefitCollection;
use Employee\Application\Query\Model\Benefit\BenefitsFinderInterface;
use Employee\Application\Query\Model\Benefit\BenefitType;
use Employee\Application\Query\Model\Department\Department;
use Employee\Application\Query\Model\Employee\Employee;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculator;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorPayload;
use Employee\Application\Salary\SalaryCalculator\SalaryCalculatorResult;
use PhpSpec\ObjectBehavior;

class SalaryReportSpec extends ObjectBehavior
{
    function let(
        SalaryCalculator $salaryCalculator,
        BenefitsFinderInterface $benefitsFinder,
    ) {
        $this->beConstructedWith($salaryCalculator, $benefitsFinder);
    }

    public function it_generate_report_for_department_with_benefit(
        SalaryCalculator $salaryCalculator,
        BenefitsFinderInterface $benefitsFinder
    ) {
        //When
        $department1 = new Department('321', 'Department1');
        $employed1 = (new \DateTimeImmutable)->modify('-3 years');
        $employee1 = new Employee('111', 'Fn1', 'Ln1', $salary1 = 1000, $employed1, $department1);
        $benefitsFinder->findAll()->shouldBeCalledOnce()->willReturn(
            new BenefitCollection(...[
                $benefit1 = new Benefit('8899', 10, new BenefitType('t1', 't'), $department1)
            ])
        );

        $salaryCalculator->calculate(
            new SalaryCalculatorPayload($salary1, $benefit1->getType()->getId(), 10, 3)
        )->shouldBeCalledOnce()->willReturn(
            $e1 = new SalaryCalculatorResult(100, 10, 110)
        );

        //Then
        $result = $this->generate((new \DateTime), $employee1);

        //Given
        $result->shouldHaveCount(1);

        $result[0]->getFirstName()->shouldBeEqualTo($employee1->getFirstName());
        $result[0]->getLastName()->shouldBeEqualTo($employee1->getLastName());
        $result[0]->getDepartmentName()->shouldBeEqualTo($department1->getName());
        $result[0]->getBenefitName()->shouldBeEqualTo($benefit1->getType()->getName());
        $result[0]->getSalary()->shouldBeEqualTo($e1->getSalary());
        $result[0]->getAddition()->shouldBeEqualTo($e1->getAddition());
        $result[0]->getTotal()->shouldBeEqualTo($e1->getTotal());
    }

    public function it_generate_report_for_department_without_benefit(
        SalaryCalculator $salaryCalculator,
        BenefitsFinderInterface $benefitsFinder
    ) {
        //When
        $department1 = new Department('654', 'Department2');
        $employed1 = (new \DateTimeImmutable)->modify('-1 years');
        $benefitsFinder->findAll()->shouldBeCalledOnce()->willReturn(new BenefitCollection());
        $employee1 = new Employee('222', 'Fn2', 'Ln2', $salary1 = 2000, $employed1, $department1);

        $salaryCalculator->calculate(
            new SalaryCalculatorPayload($salary1, '', 0, 1)
        )->shouldBeCalledOnce()->willReturn(
            $e1 = new SalaryCalculatorResult(100, 10, 110)
        );

        //Then
        $result = $this->generate((new \DateTime), $employee1);

        //Given
        $result->shouldHaveCount(1);

        $result[0]->getFirstName()->shouldBeEqualTo($employee1->getFirstName());
        $result[0]->getLastName()->shouldBeEqualTo($employee1->getLastName());
        $result[0]->getDepartmentName()->shouldBeEqualTo($department1->getName());
        $result[0]->getBenefitName()->shouldBeEqualTo('--');
        $result[0]->getSalary()->shouldBeEqualTo($e1->getSalary());
        $result[0]->getAddition()->shouldBeEqualTo($e1->getAddition());
        $result[0]->getTotal()->shouldBeEqualTo($e1->getTotal());
    }
}
