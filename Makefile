login-docker:
	docker login -u gitlab+deploy-token-657819 -p zNdt9WGU8LsiKfgeN6Z3 registry.gitlab.com/trocho/t

ONESHELL:
run: login-docker
	docker-compose -f docker/docker-compose.yml up -d

ONESHELL:
install: run
	cp -n .env.example .env
	docker exec -it php php composer.phar install
	docker exec -it php php artisan key:generate
	docker exec -it php php artisan migrate

deploy-docker: login-docker
	ansible-playbook -i hosts.local deploy-docker.yml

ONESHELL:
test:
	docker exec -it php php composer.phar run-script test
